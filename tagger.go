package tagger

import (
	"fmt"
	"reflect"
	"strings"
)

var tags = []string{
	"label",
	"placeholder",
	"type",         // input: text, number, email...
	"widget",       //input, textarea, checkbox, radio, select, text(div), link(a)
	"rows",         // 1, 2, 3 for textarea
	"validate",     // empty, phone, email
	"localStorage", // store in localStorage: true / false
	"disabled",     // true or false
}

var typeIndex map[string]map[string]field

type (
	structMeta struct {
		Fields map[string]field `json:"fields,omitempty"`
		Name   string           `json:"-"`
	}
	field map[string]string
)

func GetTags(s any) (fields map[string]field) {

	t := reflect.TypeOf(s)
	kind := t.Kind()
	if kind == reflect.Struct {
		f, ok := typeIndex[t.String()]

		if !ok {
			sm := parseTags(s)
			fields = sm.Fields

		} else {
			fields = f
		}

	}

	return fields
}

// Add UI tags from structure to global index
func Index(S ...any) {
	if typeIndex == nil {
		typeIndex = make(map[string]map[string]field)
	}

	for _, s := range S {

		tags := parseTags(s)
		typeIndex[tags.Name] = tags.Fields

	}
}

func check(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func parseTags(S any) structMeta {
	sm := structMeta{}

	t := reflect.TypeOf(S)
	sm.Name = t.String()
	sm.Fields = make(map[string]field)

	kind := t.Kind()
	count := t.NumField()

	if kind == reflect.Struct {
		for i := 0; i < count; i++ {
			f := t.Field(i)

			field := make(field)

			fieldName := f.Name

			if f.Tag != "" {

				json := f.Tag.Get("json")

				if json != "" {
					j := strings.Split(json, ",")

					if len(j) > 0 {

						jset := make(map[string]struct{})
						for _, item := range j {
							jset[item] = struct{}{}
						}

						if _, ok := jset["-"]; ok {
							continue
						} else {
							if j[0] != "" {
								fieldName = j[0]
							}
						}
					}

				}
				for _, tag := range tags {
					v, ok := f.Tag.Lookup(tag)
					if ok {
						field[tag] = v
					}
				}

				if len(field) > 0 {
					sm.Fields[fieldName] = field
				}

			}

		}
	}

	return sm

}
