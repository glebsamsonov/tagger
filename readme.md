# Tagger - add ui-specific tags to structs

## Supported tags
- label
- placehodler
- type
- widget
- rows
- validate
- localStorage
- disabled

## How to use

Add tags to a struct definition:

```go
Tool struct {
		URL         string  `json:"url" label:"Ссылка" placeholder:"Ссылка на инструмент" widget:"input" type:"text" validate:"empty"`
		Alias       string  `json:"alias" label:"Название" placeholder:"Название инструмента" widget:"input" type:"text"`
		Description string  `json:"description" label:"Описание" placeholder:"Описание инструмента" widget:"textarea" rows:"5"`
		Identifier  string  `json:"identifier" label:"Идентификатор" placeholder:"Идентификатор инструмента" widget:"input" type:"text" disabled:"true"`
		Rating      int     `json:"rating" label:"Рейтинг" widget:"input" type:"number"`
		Types       []*Type `json:"types,omitempty" gorm:"many2many:tool_type;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	}
```

This tags are useful when responding to a frontend. Add them to your response with preferred key, for example meta:

```go
func GetCrud(c *gin.Context) {
	type resp struct {
		Meta interface{}  `json:"meta,omitempty"`
		Data []model.Tool `json:"data,omitempty"`
	}

	var tools []model.Tool
	model.DB.Preload("Types").Find(&tools)

	r := resp{
		Meta: tagger.GetTags(model.Tool{}),
		Data: tools,
	}

	c.JSON(200, r)
}
```

This will produce following json:

```json
{
    "meta": {
        "alias": {
            "label": "Название",
            "placeholder": "Название инструмента",
            "type": "text",
            "widget": "input"
        },
        "description": {
            "label": "Описание",
            "placeholder": "Описание инструмента",
            "rows": "5",
            "widget": "textarea"
        },
        "identifier": {
            "disabled": "true",
            "label": "Идентификатор",
            "placeholder": "Идентификатор инструмента",
            "type": "text",
            "widget": "input"
        },
        "rating": {
            "label": "Рейтинг",
            "type": "number",
            "widget": "input"
        },
        "url": {
            "label": "Ссылка",
            "placeholder": "Ссылка на инструмент",
            "type": "text",
            "validate": "empty",
            "widget": "input"
        }
    },
	"data": {
		...
	}
}
```
